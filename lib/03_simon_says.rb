def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, number = 2)
  result = []
  number.times {result << string}
  result.join(" ")
end

def start_of_word(string, number)
  string[0...number]
end

def first_word(string)
  string.split[0]
end

def titleize(string)
  result = []
  small_words = ["and", "the", "over"]
  arr = string.split
  arr.each_with_index do |word, idx|
    if idx == 0
      result << word.capitalize
    elsif small_words.include?(word)
      result << word
    else
      result << word.capitalize
    end
  end
  result.join(" ")
end
