def add(first_parameter, second_parameter)
  first_parameter + second_parameter
end

def subtract(first_parameter, second_parameter)
  first_parameter - second_parameter
end

def sum(arr)
  return 0 if arr.length == 0
  arr.reduce(:+)
end

def multiply(arr)
  arr.reduce(:*)
end

def power(first_parameter, second_parameter)
  first_parameter ** second_parameter
end

def factorial(num)
  [1..num].reduce(:*)
end 
