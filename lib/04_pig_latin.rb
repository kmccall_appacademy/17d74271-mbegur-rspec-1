#rotate word until vowel is first letter and add ay
def pig_latin(word)
  vowels = "aeiou"
  arr = word.chars
  idx = 0
  while idx < arr.length
    new_word = arr.rotate(idx)
    if new_word[0..1] == ["q","u"]
      idx += 2
    elsif vowels.include?(new_word[0])
      return new_word.join + "ay"
    else
      idx += 1
    end
  end
end

#translates each word in a string to pig-latin 
def translate(string)
  arr = []
  string.split.each do |word|
    arr << pig_latin(word)
  end
  arr.join(" ")
end
